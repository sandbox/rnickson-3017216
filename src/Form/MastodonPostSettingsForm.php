<?php

namespace Drupal\social_post_mastodon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Post Mastodon.
 */
class MastodonPostSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_post_mastodon.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_post_mastodon.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_post_mastodon.settings');

    $form['mastodon_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Mastodon settings'),
      '#open' => TRUE,
    ];

    $form['mastodon_settings']['instance'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Mastodon instance URI'),
      '#default_value' => $config->get('instance'),
      '#description' => $this->t('The Mastodon instance that hosts accounts you want to post with'),
    ];

    $form['mastodon_settings']['client_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Key'),
      '#default_value' => $config->get('client_key'),
      '#description' => $this->t('Copy the Client Key here'),
    ];

    $form['mastodon_settings']['client_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#description' => $this->t('Copy the Clients Secret here'),
    ];

    $form['mastodon_settings']['redirect_urls'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Redirect URLs'),
      '#description' => $this->t('Copy this value to <em>Redirect URLs</em> field of your Mastodon App settings.'),
      '#default_value' => $GLOBALS['base_url'] . '/user/social-post/mastodon/auth/callback',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('social_post_mastodon.settings')
      ->set('instance', $values['instance'])
      ->set('client_key', $values['client_key'])
      ->set('client_secret', $values['client_secret'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
