<?php

namespace Drupal\social_post_mastodon\Plugin\Network;

use Lrf141\OAuth2\Client\Provider\Mastodon;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\MetadataBubblingUrlGenerator;
use Drupal\social_api\SocialApiException;
use Drupal\social_post\Plugin\Network\SocialPostNetwork;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines Social Post Mastodon Network Plugin.
 *
 * @Network(
 *   id = "social_post_mastodon",
 *   social_network = "Mastodon",
 *   type = "social_post",
 *   handlers = {
 *     "settings": {
 *        "class": "\Drupal\social_post_mastodon\Settings\MastodonPostSettings",
 *        "config_id": "social_post_mastodon.settings"
 *      }
 *   }
 * )
 */
class MastodonPost extends SocialPostNetwork implements MastodonPostInterface {

  use LoggerChannelTrait;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Render\MetadataBubblingUrlGenerator
   */
  protected $urlGenerator;

  /**
   * Mastodon connection.
   *
   * @var \Lrf141\OAuth2\Client\Provider\Mastodon
   */
  protected $connection;

  /**
   * The toot text (with optional media ids).
   *
   * @var array
   */
  protected $toot;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('url_generator'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * MastodonPost constructor.
   *
   * @param \Drupal\Core\Render\MetadataBubblingUrlGenerator $url_generator
   *   Used to generate a absolute url for authentication.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(MetadataBubblingUrlGenerator $url_generator,
                              array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              ConfigFactoryInterface $config_factory) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $config_factory);

    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  protected function initSdk() {
    $class_name = '\Lrf141\OAuth2\Client\Provider\Mastodon';
    if (!class_exists($class_name)) {
      throw new SocialApiException(sprintf('The Mastodon Library for The League OAuth2 could not be found. Class: %s.', $class_name));
    }

    /* @var \Drupal\social_post_mastodon\Settings\MastodonPostSettings $settings */
    $settings = $this->settings;

    if ($this->validateConfig($settings)) {
      $league_settings = [
        'clientId' => $settings->getClientId(),
        'clientSecret' => $settings->getClientSecret(),
        'instance' => $settings->getInstance(),
        'redirectUri' => $GLOBALS['base_url']  . '/user/social-post/mastodon/auth/callback',
        'scope' => 'write:statuses',
      ];

      // Proxy configuration data for outward proxy.
      $proxyUrl = $this->siteSettings->get('http_client_config')['proxy']['http'];
      if ($proxyUrl) {
        $league_settings = [
          'proxy' => $proxyUrl,
        ];
      }

      return new Mastodon($league_settings);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\social_api\SocialApiException
   */
  public function post() {
    if (!$this->connection) {
      throw new SocialApiException('Call post() method from its wrapper doPost()');
    }

    $post = $this->connection->post('/api/v1/statuses', $this->toot);

    if (isset($post->error)) {
      $this->getLogger('social_post_mastodon')->error($post->error);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function doPost($access_token, $access_token_secret, $toot) {
    $this->connection = $this->getSdk2($access_token, $access_token_secret);

    // Make backwards-compatible if someone just posts a toot text as a string.
    $this->toot['status'] = is_array($toot) && !empty($toot['status']) ? $toot['status'] : $toot;

    // Check if there needs to be media uploaded. If so, upload and store ids.
    if (!empty($toot['media_paths'])) {
      $this->toot['media_ids'] = $this->uploadMedia($toot['media_paths']);
    }

    return $this->post();
  }

  /**
   * {@inheritdoc}
   */
  public function getOauthCallback() {
    return $this->urlGenerator->generateFromRoute('social_post_mastodon.callback', [], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSdk2($oauth_token, $oauth_token_secret) {
    /* @var \Drupal\social_post_mastodon\Settings\MastodonPostSettings $settings */
    $settings = $this->settings;

    $league_settings = [
      'clientId' => $settings->getClientId(),
      'clientSecret' => $settings->getClientSecret(),
      'instance' => $settings->getInstance(),
      'redirectUri' => $GLOBALS['base_url']  . '/user/social-post/mastodon/auth/callback',
      'scope' => 'write:statuses',
    ];

    $provider = new Mastodon($league_settings);

    $provider->getAccessToken('authorization_code', [
      'code' => $oauth_token,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function uploadMedia(array $paths) {
    $media_ids = [];
    foreach ($paths as $path) {
      // Upload the media from the path.
      $media = $this->connection->upload('media/upload', ['media' => $path]);

      // The response contains the media_ids to attach the media to the post.
      $media_ids[] = $media->media_id_string;
    }
    return $media_ids;
  }

}
