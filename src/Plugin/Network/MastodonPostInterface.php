<?php

namespace Drupal\social_post_mastodon\Plugin\Network;

use Drupal\social_post\Plugin\Network\SocialPostNetworkInterface;

/**
 * Defines an interface for Mastodon Post Network Plugin.
 */
interface MastodonPostInterface extends SocialPostNetworkInterface {

  /**
   * Gets the absolute url of the callback.
   *
   * @return string
   *   The callback url.
   */
  public function getOauthCallback();

  /**
   * Wrapper for post method.
   *
   * @param string $access_token
   *   The access token.
   * @param string $access_token_secret
   *   The access token secret.
   * @param string|array $tweet
   *   The tweet text (with optional media paths).
   */
  public function doPost($access_token, $access_token_secret, $tweet);

  /**
   * Gets a Mastodon OAuth instance with oauth_token and oauth_token_secret.
   *
   * This method creates the SDK object by also passing the oauth_token and
   * oauth_token_secret. It is used for getting permanent tokens from
   * Mastodon and authenticating users that has already granted permission.
   *
   * @param string $oauth_token
   *   The oauth token.
   * @param string $oauth_token_secret
   *   The oauth token secret.
   *
   * @return \Lrf141\OAuth2\Client\Provider\Mastodon
   *   The instance of the connection to Mastodon.
   */
  public function getSdk2($oauth_token, $oauth_token_secret);

  /**
   * Uploads files by path.
   *
   * @param array $paths
   *   The paths for media to upload.
   */
  public function uploadMedia(array $paths);

}
