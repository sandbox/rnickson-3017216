<?php

namespace Drupal\social_post_mastodon\Settings;

use Drupal\social_api\Settings\SettingsBase;

/**
 * Returns the app information.
 */
class MastodonPostSettings extends SettingsBase implements MastodonPostSettingsInterface {

  /**
   * Client key.
   *
   * @var string
   */
  protected $clientKey;

  /**
   * Client secret.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * The Mastodon instance URL.
   *
   * @var string
   */
  protected $instance;

  /**
   * {@inheritdoc}
   */
  public function getClientKey() {
    if (!$this->clientKey) {
      $this->clientKey = $this->config->get('client_key');
    }

    return $this->clientKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret() {
    if (!$this->clientSecret) {
      $this->clientSecret = $this->config->get('client_secret');
    }

    return $this->clientSecret;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance() {
    if (!$this->instance) {
      $this->instance = $this->config->get('instance');
    }

    return $this->instance;
  }

}
