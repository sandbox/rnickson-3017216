<?php

namespace Drupal\social_post_mastodon\Settings;

/**
 * Defines an interface for Social Post Mastodon settings.
 */
interface MastodonPostSettingsInterface {

  /**
   * Gets the client key.
   *
   * @return string
   *   The client key.
   */
  public function getClientKey();

  /**
   * Gets the client secret.
   *
   * @return string
   *   The client secret.
   */
  public function getClientSecret();

  /**
   * Gets the instance URL.
   *
   * @return string
   *   The Mastodon instance URL.
   */
  public function getInstance();

}
